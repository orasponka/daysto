#!/usr/bin/env bash
if [ $(expr '(' $(date -d $1 +%s) - $(date +%s) + 86399 ')' / 86400) -gt 0 ]
then
    echo $(expr '(' $(date -d $1 +%s) - $(date +%s) + 86399 ')' / 86400)
elif [[ "$(date -d $1)" = "$(date +%s)" ]]
then
    echo $(expr '(' $(date -d $1 +%s) - $(date +%s) + 86399 ')' / 86400 + 365)
else
    echo $(expr '(' $(date -d $1 +%s) - $(date +%s) + 86399 ')' / 86400 - 1 + 365)
fi 
