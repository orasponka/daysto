# Daysto Scripts

Simple scripts that tell you how many days is to some date

## Usage

Example to get output of how many days to remain to April's 22th
```
daysto 22-Apr
```
Example to get output of how mane days to remain to Christmas Eve. <br>
'joulu' is equal to Christmas, but in Finnish
```
daystojoulu
```

## Installation
If you run Arch Linux and you have enabled op-arch-repo you can install daysto with pacman
```
sudo pacman -S daysto
```
If you run Arch Linux or Arch Linux based distro you can build package your self and install it
```
git clone https://gitlab.com/orasponka/daysto.git
cd daysto/
makepkg -si
```
Or if you don't run Arch Linux 
```
git clone https://gitlab.com/orasponka/daysto.git
cd daysto/
chmod +x *.sh
./daysto
```
